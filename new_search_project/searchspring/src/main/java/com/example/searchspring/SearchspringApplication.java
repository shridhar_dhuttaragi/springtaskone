package com.example.searchspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SearchspringApplication {

	public static void main(String[] args) {
		SpringApplication.run(SearchspringApplication.class, args);
	}

}

package com.example.forecastproject.repository;

import com.example.forecastproject.model.Main;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MainRepo  extends CrudRepository<Main,Integer>{
    @Query(value = "SELECT dl.dt_txt as date, m.MAIN_ID,m.FEELS_LIKE,m.HUMIDITY, " +
            " m.PRESSURE,m.SEA_LEVEL,m.TEMP,m.TEMP_KF,m.TEMP_MAX,m.TEMP_MIN FROM MAIN m, " +
            "DETAILS_LIST dl,FORECAST_DETAILE fd WHERE dl.MAIN_FK_ID=m.MAIN_ID AND  lower(fd.name)=lower(?1) ",nativeQuery = true)
    List<Main> fetchavgOfDailyData(String cityname);

    @Query(value = "SELECT  AVG(m.PRESSURE ) as pressure from FORECAST_DETAILE fd,MAIN m WHERE   lower(fd.name)=lower(?1) ",nativeQuery = true)
    long avgpressure(String cityname);



}

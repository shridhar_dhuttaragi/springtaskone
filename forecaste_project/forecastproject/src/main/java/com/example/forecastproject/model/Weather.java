package com.example.forecastproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Table(name = "weather_details")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Weather {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int weather_pk_id;

    int  id;
    String main;
    String description;
    String icon;

//    @ManyToOne
@Transient
Details_List details_list;

}

package com.example.forecastproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Details_List {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int details_pk_id;

    @Column
    String dt_txt;
    @Column
    int time;
    long dt;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "main_fk_id")
    Main main;
//    Clouds clouds;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "weather_fk_id")
    List<Weather> weather;

    @ManyToOne
    @Transient
    ForecastDetaile forecastDetaile;
}

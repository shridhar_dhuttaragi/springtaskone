package com.example.forecastproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class City {

    String name;
    String country;
    long population;
    long timezone;
    long sunrise;
    long sunset;

}

package com.example.forecastproject.service;

import com.example.forecastproject.model.*;
import com.example.forecastproject.repository.ForecastRepo;
import com.example.forecastproject.repository.MainRepo;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class ForecastService {
    private static final String API_KEY ="cbab3d04b7e90ab07edf5000d813576b";

    @Autowired
    ForecastRepo forecastRepo;
    @Autowired
    MainRepo mainRepo;

    public Object forecastOfCities(String cityname) throws ParseException {
        //cnt is 1day = 8
        String websiteResponse = "http://api.openweathermap.org/data/2.5/forecast?q=" + cityname +"&exclude=hourly&cnt=16&appid="+ API_KEY ;
        RestTemplate restTemplate = new RestTemplate();

        ForecastDetaile  result = restTemplate.getForObject(websiteResponse, ForecastDetaile.class);
       return forecastRepo.save(result);

    }

    public Object weatherOfCities(String cityname) {
        String websiteResponse = "http://api.openweathermap.org/data/2.5/weather?q=" + cityname +"&appid="+ API_KEY ;
        RestTemplate restTemplate = new RestTemplate();
        Details_List weatherDetails = restTemplate.getForObject(websiteResponse, Details_List.class);
        return weatherDetails;
    }

    public Object avgdaily(String cityname) {
        int c= forecastRepo.existsByIdName(cityname);
        if(c>=1)
        {
            return c;
        }else
        {
            return null;
        }

    }

    public Object avgOfPressure(String cityname) {
        Main main=new Main();
        int c= forecastRepo.existsByIdName(cityname);
        if(c>=1)
        {
            long presure= mainRepo.avgpressure(cityname);

            System.out.println(presure);
            main.setPressure(presure);
//            List<Main> M= mainRepo.fetchavgOfDailyData(cityname);

            return main;
        }else
        {
            return "NO data.";
        }
    }

    public Object averageOfDaily(String cityname) throws ParseException {
        int c= forecastRepo.existsByIdName(cityname);
        if(c>=1)
        {
            List<Main> M= mainRepo.fetchavgOfDailyData(cityname);
            int temprature=0;
            SimpleDateFormat dfinput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                for (Main o:M) {
                    String hh= o.getDt_txt();
//                    System.out.println(hh);
                    java.util.Date date1 = dfinput.parse(hh);
                    int h=date1.getHours();

                    if(h>=06&&h<18)
                    {
                        temprature+=o.getTemp();
                    }else
                    {
//                        System.out.println("dsf");
                    }
//                System.out.println(temprature);
            }
                Main main=new Main();
                main.setTemp((long)temprature/12);
            return main;

        }else
        {
            return "no data";
        }
    }

    public Object averageOfNightly(String cityname) throws ParseException {
        int c= forecastRepo.existsByIdName(cityname);
        if(c>=1)
        {
            List<Main> M= mainRepo.fetchavgOfDailyData(cityname);
            int temprature=0;
            SimpleDateFormat dfinput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            for (Main o:M) {
                String hh= o.getDt_txt();
//                    System.out.println(hh);
                java.util.Date date1 = dfinput.parse(hh);
                int h=date1.getHours();
                if(h>=18|| h<06)
                {
                    temprature+=o.getTemp();
                }else
                {
//                        System.out.println("dsf");
                }
//                System.out.println(temprature);
            }
            Main main=new Main();
            main.setTemp((long)temprature/12);
            return main;
        }else
        {
            return "no data";
        }
    }
}

package com.example.forecastproject.repository;

import com.example.forecastproject.model.City;
import com.example.forecastproject.model.ForecastDetaile;
import com.example.forecastproject.model.Main;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ForecastRepo extends CrudRepository<ForecastDetaile,Integer> {

    @Query(value = "SELECT count(*) from  FORECAST_DETAILE WHERE lower(name) =lower(?1)",nativeQuery = true)
    int existsByIdName(String cityname);


}
